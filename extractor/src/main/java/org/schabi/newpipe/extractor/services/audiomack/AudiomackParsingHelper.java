package org.schabi.newpipe.extractor.services.audiomack;

import org.jsoup.Jsoup;
import org.schabi.newpipe.extractor.NewPipe;
import org.schabi.newpipe.extractor.exceptions.ParsingException;
import org.schabi.newpipe.extractor.exceptions.ReCaptchaException;
import org.schabi.newpipe.extractor.utils.Parser;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.*;

public class AudiomackParsingHelper {

    private AudiomackParsingHelper() {
    }

    /**
     * Fetch the embed player with the url and return the id (like the id from the json api).
     *
     * @return the resolved id
     */
    public static String resolveIdWithApi(String url) throws IOException, ReCaptchaException, ParsingException {
	// SONGS
    //def _real_extract(self, url):
    //    # URLs end with [uploader name]/[uploader title]
    //    # this title is whatever the user types in, and is rarely
    //    # the proper song title.  Real metadata is in the api response
    //    album_url_tag = self._match_id(url)

    //    # Request the extended version of the api for extra fields like artist and title
    //    api_response = self._download_json(
    //        'http://www.audiomack.com/api/music/url/song/%s?extended=1&_=%d' % (
    //            album_url_tag, time.time()),
    //        album_url_tag)

    //    # API is inconsistent with errors
    //    if 'url' not in api_response or not api_response['url'] or 'error' in api_response:
    //        raise ExtractorError('Invalid url %s' % url)

    //    # Audiomack wraps a lot of soundcloud tracks in their branded wrapper
    //    # if so, pass the work off to the soundcloud extractor
    //    if SoundcloudIE.suitable(api_response['url']):
    //        return self.url_result(api_response['url'], SoundcloudIE.ie_key())

    //    return {
    //        'id': compat_str(api_response.get('id', album_url_tag)),
    //        'uploader': api_response.get('artist'),
    //        'title': api_response.get('title'),
    //        'url': api_response['url'],

    // ALBUMS
    //def _real_extract(self, url):
    //    # URLs end with [uploader name]/[uploader title]
    //    # this title is whatever the user types in, and is rarely
    //    # the proper song title.  Real metadata is in the api response
    //    album_url_tag = self._match_id(url)
    //    result = {'_type': 'playlist', 'entries': []}
    //    # There is no one endpoint for album metadata - instead it is included/repeated in each song's metadata
    //    # Therefore we don't know how many songs the album has and must infi-loop until failure
    //    for track_no in itertools.count():
    //        # Get song's metadata
    //        api_response = self._download_json(
    //            'http://www.audiomack.com/api/music/url/album/%s/%d?extended=1&_=%d'
    //            % (album_url_tag, track_no, time.time()), album_url_tag,
    //            note='Querying song information (%d)' % (track_no + 1))

    //        # Total failure, only occurs when url is totally wrong
    //        # Won't happen in middle of valid playlist (next case)
    //        if 'url' not in api_response or 'error' in api_response:
    //            raise ExtractorError('Invalid url for track %d of album url %s' % (track_no, url))
    //        # URL is good but song id doesn't exist - usually means end of playlist
    //        elif not api_response['url']:
    //            break
    //        else:
    //            # Pull out the album metadata and add to result (if it exists)
    //            for resultkey, apikey in [('id', 'album_id'), ('title', 'album_title')]:
    //                if apikey in api_response and resultkey not in result:
    //                    result[resultkey] = api_response[apikey]
    //            song_id = url_basename(api_response['url']).rpartition('.')[0]
    //            result['entries'].append({
    //                'id': compat_str(api_response.get('id', song_id)),
    //                'uploader': api_response.get('artist'),
    //                'title': api_response.get('title', song_id),
    //                'url': api_response['url'],
    //            })
    //    return result
        return "5298094";
    }
}
