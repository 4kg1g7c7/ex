package org.schabi.newpipe.extractor.services.audiomack;

import org.schabi.newpipe.extractor.exceptions.ParsingException;
import org.schabi.newpipe.extractor.linkhandler.SearchQueryHandlerFactory;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

public class AudiomackSearchQueryHandlerFactory extends SearchQueryHandlerFactory {

    public static final String ALL = "all";
    public static final String PLAYLISTS = "playlists";
    public static final String ARTISTS = "artists";
    public static final String SONGS = "songs";
    public static final String ALBUMS = "albums";

    //sort: relevant, popular, recent
    //sort: genre

    @Override
    public String[] getAvailableContentFilter() {
        return new String[] {
                ALL,
                PLAYLISTS,
                ARTISTS,
                SONGS,
                ALBUMS};
    }

    @Override
    public String[] getAvailableSortFilter() {
        return new String[0];
    }

    @Override
    public String getUrl(String query, List<String> contentFilter, String sortFilter) throws ParsingException {
        try {
            return "https://www.audiomack.com/api/music/search/?q=" + URLEncoder.encode(query, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new ParsingException("Could not create search string with query: " + query, e);
        }
    }
}
