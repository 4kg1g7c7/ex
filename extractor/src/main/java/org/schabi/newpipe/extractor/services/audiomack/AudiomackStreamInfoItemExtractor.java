package org.schabi.newpipe.extractor.services.audiomack;

import com.grack.nanojson.JsonObject;
import org.schabi.newpipe.extractor.exceptions.ParsingException;
import org.schabi.newpipe.extractor.localization.DateWrapper;
import org.schabi.newpipe.extractor.stream.StreamInfoItemExtractor;
import org.schabi.newpipe.extractor.stream.StreamType;

import static org.schabi.newpipe.extractor.utils.Utils.replaceHttpWithHttps;

public class AudiomackStreamInfoItemExtractor implements StreamInfoItemExtractor {

    protected final JsonObject itemObject;

    public AudiomackStreamInfoItemExtractor(JsonObject itemObject) {
        this.itemObject = itemObject;
    }

    @Override
    public String getUrl() {
        return "https://audiomack.com/api/music/url/song/iamfallow/chills";
    }

    @Override
    public String getName() {
        return itemObject.getString("title");
    }

    @Override
    public long getDuration() {
        return itemObject.getInt("duration", 0);
    }

    @Override
    public String getUploaderName() {
        return itemObject.getString("uploader");
    }

    @Override
    public String getUploaderUrl() {
        return "https://audiomack.com/artist/" + itemObject.getString("uploader");
    }

    @Override
    public String getTextualUploadDate() {
        return null;
    }

    @Override
    public DateWrapper getUploadDate() throws ParsingException {
        return null;
    }

    private String getCreatedAt() {
        return null;
    }

    @Override
    public long getViewCount() {
        return 0;
    }

    @Override
    public String getThumbnailUrl() {
        return null;
    }

    @Override
    public StreamType getStreamType() {
        return StreamType.AUDIO_STREAM;
    }

    @Override
    public boolean isAd() {
        return false;
    }
}
