package org.schabi.newpipe.extractor.services.audiomack;

import org.schabi.newpipe.extractor.linkhandler.LinkHandlerFactory;
import org.schabi.newpipe.extractor.exceptions.ParsingException;
import org.schabi.newpipe.extractor.utils.Parser;
import org.schabi.newpipe.extractor.utils.Utils;

public class AudiomackStreamLinkHandlerFactory extends LinkHandlerFactory {
    private static final AudiomackStreamLinkHandlerFactory instance = new AudiomackStreamLinkHandlerFactory();
    private final String URL_PATTERN = "^https?://(wwww.)?audiomack.com/song/[0-9a-z_-]+/?([#?].*)?$";

    private AudiomackStreamLinkHandlerFactory() {
    }

    public static AudiomackStreamLinkHandlerFactory getInstance() {
        return instance;
    }

    @Override
    public String getUrl(String id) throws ParsingException {
        return null;
    }

    @Override
    public String getId(String url) throws ParsingException {
        Utils.checkUrl(URL_PATTERN, url);

        try {
            return AudiomackParsingHelper.resolveIdWithApi(url);
        } catch (Exception e) {
            throw new ParsingException(e.getMessage(), e);
        }
    }

    @Override
    public boolean onAcceptUrl(final String url) throws ParsingException {
        return Parser.isMatch(URL_PATTERN, url.toLowerCase());
    }
}
